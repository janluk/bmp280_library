# BMP280 library (Tested on STM32 NUCLEO-F411RE)

* [General info](#general-info)
* [Setup](#setup)
* [additional functions](#additional-functions)
* [Author and license](#author-and-license)


# **General info** 
The BMP280 is an absolute barometric pressure and temperature sensor especially designed for mobile
applications. The sensor module is housed in an extremely compact 8-pin metal-lid LGA
package with a footprint of only 2.0 × 2.5 mm2 and 0.95 mm package height. Its small
dimensions and its low power consumption of 2.7 μA @1Hz allow the implementation in battery
driven devices such as mobile phones, GPS modules or watches.

# **Setup** 


## [1] Include files
```C
#include "bmp280.h"
#include "bmp280_interface.h"

#define BMP280_I2C_ADDRESS_1 0x76
#define BMP280_I2C_ADDRESS_2 0x77
```
## [2] Driver structure
```C
typedef struct
{
	/**
	 * Reads 8 bits from register address
	 *
	 * @param *bmp BMP280 strcture
	 * @param ReadFromRegisterAddress Addres of BMP280 register to read
	 * @return Returns read one byte from the register
	 */
	    uint8_t (*Read8Bits)(bmp280_t *bmp280, uint8_t ReadFromRegisterAddress);


	/**
	 * Writes 8 bits to indicated register
	 *
	 * @param *bmp BMP280 strcture.
	 * @param WriteToRegister Addres of BMP280 register to write
	 * @param ValueForRegister Data to writes to the register
	 */
	    void (*Write8Bits)(bmp280_t *bmp280, uint8_t WriteToRegisterAddress, uint8_t ValueForRegister);


	/**
	 * Reads 24 bits from register address
	 *
	 * @param *bmp BMP280 strcture
	 * @param ReadFromRegisterAddress Addres of BMP280 register to read
	 * @return Returns read 24 bits from the register
	 *
	 * @note returned value should by:
	 * \code{.c}
	 *
	 * | FF		| FF 	| F0 	|
	 * | MSB	| LSB 	| xLSB 	|
	 *
	  * return ((Value[0] << 16) | (Value[1] << 8) | Value[2]);
	 * \endcode
	 */
	    uint32_t (*Read24Bits)(bmp280_t *bmp280, uint8_t ReadFromRegisterAddress);


	/**
	 * Reads 24 bits from register address
	 *
	 * @param *bmp BMP280 strcture
	 * @param ReadFromRegisterAddress Addres of BMP280 register to read
	 * @param  Write[24] Returns the 24 bytes read from the register to the indicated 24-element array using a pointer
	 */
	    void (*Read24Bytes)(bmp280_t *bmp280, uint8_t ReadFromRegisterAddress, uint8_t WriteData[24]);

}bmp280_driver_t;
```
</br>

<details>
<summary>Click here to see <b>example for i2c STM32 HAL  implementation</b></summary>


```C
/**
 * \ingroup INTERFACE
 *
 *
 */
#include "bmp280_interface.h"
#include "i2c.h"
#include "stdint.h"

static bmp280_driver_t Bmp280Driver ={
		bmp280Read8,
		bmp280Write8,
		bmp280Read24Bits,
		bmp280Read24Bytes,
};

bmp280_driver_t *bmp280_GetDriver(void)
{
	return &Bmp280Driver;
}

uint8_t bmp280Read8(bmp280_t *bmp, uint8_t ReadFromRegisterAddress)
{
	uint8_t Value;

	bmp->ErrorState = HAL_I2C_Mem_Read(bmp->bmp_i2c, ((bmp->address_i2c)<<1), ReadFromRegisterAddress, 1, &Value, 1,BMP280_I2C_TIMEOUT);

	return Value;
}

void  bmp280Write8(bmp280_t *bmp, uint8_t WriteToRegister, uint8_t ValueForRegister)
{
	bmp->ErrorState = HAL_I2C_Mem_Write(bmp->bmp_i2c, ((bmp->address_i2c)<<1), WriteToRegister, 1, &ValueForRegister, 1, BMP280_I2C_TIMEOUT);
}

uint32_t bmp280Read24Bits(bmp280_t *bmp, uint8_t ReadFromRegisterAddress)
{
	uint8_t Value[3];

	bmp->ErrorState = HAL_I2C_Mem_Read(bmp->bmp_i2c, ((bmp->address_i2c)<<1), ReadFromRegisterAddress, 1, Value, 3, BMP280_I2C_TIMEOUT);

	return ((Value[0] << 16) | (Value[1] << 8) | Value[2]);
}

void  bmp280Read24Bytes(bmp280_t *bmp, uint8_t ReadFromRegisterAddress, uint8_t Write[24])
{
	bmp->ErrorState =  HAL_I2C_Mem_Read(bmp->bmp_i2c, ((bmp->address_i2c)<<1), BMP280_DIG_T1, 1, Write, 24, BMP280_I2C_TIMEOUT);
}


```  

</details>


## [3] Initialize structures

 Main structures:

```C
bmp280_t bmp280_1;
bmp280_t bmp280_2;
```
## [4] Helping variables

```C
float Temperature_1, Pressure_1; 
float Temperature_2, Pressure_2;

bmp280_status_t bmp280_1InitState;
bmp280_status_t bmp280_2InitState;

uint8_t bmp280Settings_1[3];
uint8_t bmp280Settings_2[3];

uint32_t bmp280SoftTimer;
```

## [5] Initialized library

```C
  if((bmp280_LinkDriver(bmp280_GetDriver())) == bmp280_NoError)
  {
	  bmp280_1InitState = bmp280_init(&bmp280_1, &hi2c1, BMP280_I2C_ADDRESS_1);

	  bmp280_2InitState = bmp280_init(&bmp280_2, &hi2c1, BMP280_I2C_ADDRESS_2);
  }
  else
  {
	  //link driver error
  }

  bmp280SoftTimer = HAL_GetTick();
```

## [6] Main loop:
```C
    if ((HAL_GetTick() - bmp280SoftTimer) >= 100) 
    {
        bmp280SoftTimer = HAL_GetTick();

        if (bmp280_1InitState == bmp280_NoError)
	    {
            BMP280_ReadPressureAndTemperature(&bmp280_1, &Pressure_1, &Temperature_1);
            //Temperature_1 = BMP280_ReadTemperature(&bmp280_1);
	    }
    }
	  
    if ((HAL_GetTick() - bmp280SoftTimer) >= 100) 
    {
        bmp280SoftTimer = HAL_GetTick();
        
	    if (bmp280_2InitState == bmp280_NoError)
	    {
		    BMP280_ReadPressureAndTemperature(&bmp280_2, &Pressure_2, &Temperature_2);
            //Temperature_2 = BMP280_ReadTemperature(&bmp280_2);
	    }
    }

```

# **Additional functions**

```C
bmp280_SetTemperatureOversampling(&bmp280_1, BMP280_TEMPERATURE_19BIT);
bmp280_SetPressureOversampling(&bmp280_1, BMP280_ULTRAHIGHRES);
bmp280_SetMode(&bmp280_1, BMP280_NORMALMODE);

bmp280_SetStandbyTime(&bmp280_1,BMP280_STANDBY_MS_4000);
bmp280_Set3WireSPI(&bmp280_1,BMP280_3WireSPI_ON);
bmp280_SetIIRFilter(&bmp280_1,BMP280_FILTER_X8);

bmp280Settings_1[0] = bmp280_GetMode(&bmp280_1);
bmp280Settings_1[1] = bmp280_GetTemperatureOversampling(&bmp280_1) ;
bmp280Settings_1[2] = bmp280_GetPressureOversampling(&bmp280_1);
```
**More doxygen documentation** in doc/html/index.html

# **Author and license**

* Jan Łukaszewicz pldevluk@gmail.com

The MIT License (MIT)

Copyright 2022-present  <COPYRIGHT HOLDER>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
