/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "i2c_scan.h"
#include "bmp280.h"
#include "bmp280_interface.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BMP280_I2C_ADDRESS_1 0x76
#define BMP280_I2C_ADDRESS_2 0x77

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
bmp280_t bmp280_1, bmp280_2;

float Temperature_1, Pressure_1;
float Temperature_2, Pressure_2;

bmp280_status_t bmp280_1InitState;
bmp280_status_t bmp280_2InitState;

uint8_t bmp280Settings_1[3];
uint8_t bmp280Settings_2[3];

uint32_t bmp280SoftTimer;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  i2c_scan_start();

  if((bmp280_LinkDriver(bmp280_GetDriver())) == bmp280_NoError)
  {
	  bmp280_1InitState = bmp280_init(&bmp280_1, &hi2c1, BMP280_I2C_ADDRESS_1);

	  bmp280_2InitState = bmp280_init(&bmp280_2, &hi2c1, BMP280_I2C_ADDRESS_2);
//  bmp280ErrorState = bmp280_SetTemperatureOversampling(&bmp280, BMP280_TEMPERATURE_19BIT);
//  bmp280ErrorState = bmp280_SetPressureOversampling(&bmp280, BMP280_ULTRAHIGHRES);
//  bmp280ErrorState = bmp280_SetMode(&bmp280, BMP280_NORMALMODE);
//
//  bmp280ErrorState = bmp280_SetStandbyTime(&bmp280,BMP280_STANDBY_MS_4000);
//  bmp280ErrorState = bmp280_Set3WireSPI(&bmp280,BMP280_3WireSPI_ON);
//  bmp280ErrorState = bmp280_SetIIRFilter(&bmp280,BMP280_FILTER_X8);

//	  bmp280Settings_1[0] = bmp280_GetMode(&bmp280_1);
//	  bmp280Settings_1[1] = bmp280_GetTemperatureOversampling(&bmp280_1) ;
//	  bmp280Settings_1[2] = bmp280_GetPressureOversampling(&bmp280_1);
  }
  else
  {
	  //link driver error
  }

  bmp280SoftTimer = HAL_GetTick();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	    if ((HAL_GetTick() - bmp280SoftTimer) >= 100)
	    {
	    	bmp280SoftTimer = HAL_GetTick();

	        if (bmp280_1InitState == bmp280_NoError)
		    {
	            BMP280_ReadPressureAndTemperature(&bmp280_1, &Pressure_1, &Temperature_1);
	            //Temperature_1 = BMP280_ReadTemperature(&bmp280_1);
		    }
	    }

	    if ((HAL_GetTick() - bmp280SoftTimer) >= 100)
	    {
	    	bmp280SoftTimer = HAL_GetTick();

		    if (bmp280_2InitState == bmp280_NoError)
		    {
			    BMP280_ReadPressureAndTemperature(&bmp280_2, &Pressure_2, &Temperature_2);
	            //Temperature_2 = BMP280_ReadTemperature(&bmp280_2);
		     }
	    }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
