/*
 * i2c_scan.c
 *
 *  Created on: Apr 22, 2023
 *  @author Jan Łukaszewicz
 */
#include "i2c_scan.h"

#include "usart.h"
#include "i2c.h"

#include <stdio.h>



void  i2c_scan_start(void)
{
	 char uart2Data[24] = "Connected to UART Two\r\n";
	 HAL_UART_Transmit(&huart2, (uint8_t *)&uart2Data,sizeof(uart2Data), 0xFFFF);

	  	printf("\r\n");

	  	printf("Scanning I2C bus:\r\n");
	 	HAL_StatusTypeDef result;
	  	uint8_t i;
	  	for (i=1; i<128; i++)
	  	{
	  	  /*
	  	   * the HAL wants a left aligned i2c address
	  	   * &hi2c1 is the handle
	  	   * (uint16_t)(i<<1) is the i2c address left aligned
	  	   * retries 2
	  	   * timeout 2
	  	   */
	  	  result = HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t)(i<<1), 2, 2);
	  	  if (result != HAL_OK) // HAL_ERROR or HAL_BUSY or HAL_TIMEOUT
	  	  {
	  		  printf("."); // No ACK received at that address
	  	  }
	  	  if (result == HAL_OK)
	  	  {
	  		  printf("0x%X", i); // Received an ACK at that address
	  	  }
	  	}
	  	printf("\r\n");
}

PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART2 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}
