/**
 * \defgroup int_i2c I2C Interface
 * \ingroup INTERFACE
 *
 * \author Jan Łukaszewicz
 * \date 2023.05.10
 *
 * @{
 * Interface for STM32 I2C communication. Uses the HAL function
 */

#ifndef BMP280_INTERFACE_H_
#define BMP280_INTERFACE_H_

#include "bmp280.h"

/**
 *  Function returns driver address.
 */
bmp280_driver_t *bmp280_GetDriver(void);

/**
 * Reads 8 bits from register address
 *
 * @param *bmp BMP280 strcture
 * @param ReadFromRegisterAddress Addres of BMP280 register to read
 * @return Returns read one byte from the register
 */
uint8_t bmp280Read8(bmp280_t *bmp, uint8_t ReadFromRegisterAddress);

/**
 * Writes 8 bits to indicated register
 *
 * @param *bmp BMP280 strcture.
 * @param WriteToRegister Addres of BMP280 register to write
 * @param ValueForRegister Data to writes to the register
 */
void bmp280Write8(bmp280_t *bmp, uint8_t WriteToRegister, uint8_t ValueForRegister);

/**
 * Reads 24 bits from register address
 *
 * @param *bmp BMP280 strcture
 * @param ReadFromRegisterAddress Addres of BMP280 register to read
 * @return Returns read 24 bits from the register
 *
 * @note returned value should by:
 * \code{.c}
 *
 * | FF		| FF 	| F0 	|
 * | MSB	| LSB 	| xLSB 	|
 *
  * return ((Value[0] << 16) | (Value[1] << 8) | Value[2]);
 * \endcode
 */
uint32_t bmp280Read24Bits(bmp280_t *bmp, uint8_t ReadFromRegisterAddress);

/**
 * Reads 24 bits from register address
 *
 * @param *bmp BMP280 strcture
 * @param ReadFromRegisterAddress Addres of BMP280 register to read
 * @param  Write[24] Returns the 24 bytes read from the register to the indicated 24-element array using a pointer
 */
void  bmp280Read24Bytes(bmp280_t *bmp, uint8_t ReadFromRegisterAddress, uint8_t Write[24]);

#endif /* BMP280_INTERFACE_H_ */
/// @}
