/**
 * \ingroup INTERFACE
 *
 *
 */
#include "bmp280_interface.h"
#include "i2c.h"
#include "stdint.h"

static bmp280_driver_t Bmp280Driver ={
		bmp280Read8,
		bmp280Write8,
		bmp280Read24Bits,
		bmp280Read24Bytes,
};

bmp280_driver_t *bmp280_GetDriver(void)
{
	return &Bmp280Driver;
}

uint8_t bmp280Read8(bmp280_t *bmp, uint8_t ReadFromRegisterAddress)
{
	uint8_t Value;

	bmp->ErrorState = HAL_I2C_Mem_Read(bmp->bmp_i2c, ((bmp->address_i2c)<<1), ReadFromRegisterAddress, 1, &Value, 1,BMP280_I2C_TIMEOUT);

	return Value;
}

void  bmp280Write8(bmp280_t *bmp, uint8_t WriteToRegister, uint8_t ValueForRegister)
{
	bmp->ErrorState = HAL_I2C_Mem_Write(bmp->bmp_i2c, ((bmp->address_i2c)<<1), WriteToRegister, 1, &ValueForRegister, 1, BMP280_I2C_TIMEOUT);
}

uint32_t bmp280Read24Bits(bmp280_t *bmp, uint8_t ReadFromRegisterAddress)
{
	uint8_t Value[3];

	bmp->ErrorState = HAL_I2C_Mem_Read(bmp->bmp_i2c, ((bmp->address_i2c)<<1), ReadFromRegisterAddress, 1, Value, 3, BMP280_I2C_TIMEOUT);

	return ((Value[0] << 16) | (Value[1] << 8) | Value[2]);
}

void  bmp280Read24Bytes(bmp280_t *bmp, uint8_t ReadFromRegisterAddress, uint8_t Write[24])
{
	bmp->ErrorState =  HAL_I2C_Mem_Read(bmp->bmp_i2c, ((bmp->address_i2c)<<1), BMP280_DIG_T1, 1, Write, 24, BMP280_I2C_TIMEOUT);
}

