/*
 * i2c_scan.h
 *
 *  Created on: Apr 22, 2023
 *  @author Jan Łukaszewicz
 */

#ifndef I2C_SCAN_H_
#define I2C_SCAN_H_




#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

#ifndef __UUID_H
#define __UUID_H
//#define STM32_UUID ((uint32_t *)0x1FF0F420)
#define STM32_UUID ((uint32_t *)UID_BASE)
#endif //__UUID_H

/**
 * @ingroup SCAN
 * @brief Startin i2c scan. Result will be transfer by uart. Library works with STM32, it using HAL framework
 *
 */
void  i2c_scan_start(void);

#endif /* I2C_SCAN_H_ */
