/*
 * bmp280.h
 *
 *  author Jan Łukaszewicz
 *  date 2023.05.10
 *
 */

#ifndef BMP280_H_
#define BMP280_H_

#include "stdint.h"
#include "i2c.h"

/**
 * @defgroup DEF_GUARD Guard
 * @ingroup DEFINES DEF_GUARD
 * @{
 *
 *  Checking pointer of driver
 */
#define POINTER_GUARD_OF_DRIVER if(NULL == driver_bmp280) {return bmp280_ErrorLinkDriver;}

/**
 *  Checking pointer of driver callback
 *  @param fn We provide the pointer to the callback. Example: POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read24Bytes)
 */
#define POINTER_GUARD_OF_DRIVER_CALLBACK(fn) if((NULL == Bmp280Driver) || (NULL == (fn))) {return bmp280_ErrorCallbackPointer;}

/**
 *  Checking error
 *  @param chk We provide error state
 *  @param out We return an error state
 */
#define ERROR_CHECK(chk, out) if(0 < (chk)){return (out);}


/**
 * @ingroup DEFINES
 *  I2C timeout
 */
#define BMP280_I2C_TIMEOUT	1000

/**
 *  @defgroup DEFINES Defines
 * @ingroup BMP280
 *
 * @defgroup DEF_BITS Operation on bits
 *  @ingroup DEFINES
 *  @{
 */
#define SET_BIT(REG, BIT)     ((REG) |= (BIT))
#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))
/** @} */ // end of DEF_BITS

/**
 * @defgroup DEFINES Defines
 * @ingroup BMP280
 *
 * @defgroup DEF_TEMP Temperature resolution
 *  @ingroup DEFINES
 *  @{
 */
#define BMP280_DISABLE_TEMPERATURE 0
#define BMP280_TEMPERATURE_16BIT 1
#define BMP280_TEMPERATURE_17BIT 2
#define BMP280_TEMPERATURE_18BIT 3
#define BMP280_TEMPERATURE_19BIT 4
#define BMP280_TEMPERATURE_20BIT 5
#define BMP280_MAX_REG_VALUE_TEMPERATURE 5
/** @} */ // end of DEF_TEMP

/**
 * @defgroup DEF_PRESSURE Pressure oversampling
 *  @ingroup DEFINES
 *  @{
 */
#define BMP280_DISABLE_PRESSURE 0
#define BMP280_ULTRALOWPOWER	1
#define BMP280_LOWPOWER			2
#define BMP280_STANDARD			3
#define BMP280_HIGHRES			4
#define BMP280_ULTRAHIGHRES		5
#define BMP280_MAX_REG_VALUE_PRESSURE 5
/** @} */ // end of DEF_PRESSURE


/**
 * @defgroup DEF_STANDBY Standby time
 *  @ingroup DEFINES
 *  Standby time in sleep mode
 *  @{
 */
#define BMP280_STANDBY_MS_0_5	0
#define BMP280_STANDBY_MS_62_5	1
#define BMP280_STANDBY_MS_125	2
#define BMP280_STANDBY_MS_250 	3
#define BMP280_STANDBY_MS_500	4
#define BMP280_STANDBY_MS_1000	5
#define BMP280_STANDBY_MS_2000	6
#define BMP280_STANDBY_MS_4000	7
#define BMP280_MAX_STANDBY 		7
/** @} */ // end of DEF_STANDBY

/**
 * @defgroup DEF_IIR IIR filter
 *  @ingroup DEFINES
 *  @{
 */
#define BMP280_FILTER_OFF	0
#define BMP280_FILTER_X2 	1
#define BMP280_FILTER_X4 	2
#define BMP280_FILTER_X8	3
#define BMP280_FILTER_X16 	4
#define BMP280_MAX_IIRFILTER 4
/** @} */ // end of DEF_IIR

/**
 * @defgroup DEF_3WireSPI 3-Wire SPI
 *  @ingroup DEFINES
 *  @{
 */
#define BMP280_3WireSPI_ON 1
#define BMP280_3WireSPI_OFF 0
#define BMP280_MAX_3WireSPI 1
/** @} */ // end of

/**
 * @defgroup DEF_MODE Mode
 *  @ingroup DEFINES
 *  @{
 */
#define BMP280_SLEEPMODE			0		///< In sleep mode, no measurements are performed.
#define BMP280_FORCEDMODE		1		///< In forced mode, a single measurement is performed. When the measurement is finished, the sensor returns to sleep mode.
#define BMP280_NORMALMODE		3		///< Normal mode comprises an automated perpetual cycling between an active measurement period and an inactive standby period.
#define BMP280_MAX_REG_VALUE_Mode 3
/** @} */ // end of DEF_MODE

/**
 * @defgroup DEF_COEFFS Coeffs registers
 *  @ingroup DEFINES
 *  @{
 */
#define	BMP280_DIG_T1		0x88
#define	BMP280_DIG_T2		0x8A
#define	BMP280_DIG_T3		0x8C

#define	BMP280_DIG_P1		0x8E
#define	BMP280_DIG_P2		0x90
#define	BMP280_DIG_P3		0x92
#define	BMP280_DIG_P4		0x94
#define	BMP280_DIG_P5		0x96
#define	BMP280_DIG_P6		0x98
#define	BMP280_DIG_P7		0x9A
#define	BMP280_DIG_P8		0x9C
#define	BMP280_DIG_P9		0x9E
/** @} */ // end of DEF_COEFFS

/**
 * @defgroup DEF_REG_ADDR register addresses
 *  @ingroup DEFINES
 *  @{
 */
#define	BMP280_CHIPID			0xD0
#define	BMP280_VERSION			0xD1
#define	BMP280_SOFTRESET		0xE0
#define	BMP280_CAL26			0xE1  ///< R calibration stored in 0xE1-0xF0
#define	BMP280_STATUS			0xF3
#define	BMP280_CONTROL		0xF4
#define	BMP280_CONFIG			0xF5
#define	BMP280_PRESSUREDATA	0xF7
#define	BMP280_TEMPDATA		0xFA
/** @} */ // end of DEF_REG_ADDR

/**
 * @ingroup DEFINES
 *  Control bits
 */
#define	BMP280_MEASURING			(1<<3) ///< Conversion in progress

/**
 * @ingroup BMP280
 *   Error state
 */
typedef enum
{
	bmp280_NoError = 0, 			///< 0 No Errors
	bmp280_HAL_ERROR,			///< 1 HAL Errors
	bmp280_HAL_BUSY,				///< 2 HAL Transmision is Busy
	bmp280_HAL_TIMEOUT,			///< 3 HAL Transmision timeout
	bmp280_ErrorWriteToRegister,	///< 4 Registry write error
	bmp280_ErrorChipID,			///< 5 Wrong or damaged BMP sensor
	bmp280_ErrorLinkDriver,		///< 6 Driver link error
	bmp280_ErrorCallbackPointer,	///< 7 Callback pointer error
	bmp280_ErrorRead,				///< 8 Read from registry BMP error
	bmp280_ErrorWrite,				///< 9 Write to registry BMP error
}bmp280_status_t;

/**
 * @ingroup BMP280
 *   Settings structure
 */
typedef struct
{
	/**
	 *  @name  BMP280_CONTROL ctrl_meas 0xF4
	 * @{ */
	uint8_t Mode;
	uint8_t Pressure;
	uint8_t Temerature;
	/// @}

	 /**
	 * @name BMP280_CONFIG config 0xF5
	 * @{*/
	uint8_t Spi3wireEnable;		/// 1 enabled
	uint8_t IIRFilter;			/// IIR filter
	uint8_t StandbyDuration;
	/// @}
}bmp280_settings_t;

/**
 * @ingroup BMP280
 */
typedef struct
{
	/**
	 * @name Coefficients for calculations
	 * @{ */
	uint16_t t1, p1;
	int16_t t2, t3, p2, p3, p4, p5, p6, p7, p8, p9;
	/// @}

	uint8_t 				address_i2c; 	/// BMP280 Address

	bmp280_status_t 		ErrorState;
	bmp280_settings_t	 settings;
	int32_t 				t_fine; 		/// in documentation BMP280_S32_t t_fine;
	I2C_HandleTypeDef 	*bmp_i2c;  // todo it is not supported to another MCU I will have to fix it

}bmp280_t;

#include "bmp280_driver.h" // #include must be here because the driver must have a known bmp280_t structure

/**
 * @ingroup BMP280
 * @brief BMP280 library initialization.
 *
 * Before the library initialization, you have to link driver:
 * @code
 * bmp280ErrorState = bmp280_LinkDriver(bmp280_GetDriver());
 * bmp280ErrorState = bmp280_init(&bmp280, &hi2c1, BMP280_ADDRESS);
 * @endcode
 *
 * You can also setup your own BMP280 settings such as:
 * @code
 * bmp280ErrorState = bmp280_SetMode(&bmp280, BMP280_NORMALMODE);
 * bmp280ErrorState = bmp280_SetTemperatureOversampling(&bmp280, BMP280_TEMPERATURE_19BIT);
 * bmp280ErrorState = bmp280_SetPressureOversampling(&bmp280, BMP280_ULTRAHIGHRES);
 *
 * bmp280ErrorState = bmp280_SetStandbyTime(&bmp280, BMP280_STANDBY_MS_0_5);
 * bmp280ErrorState = bmp280_SetIIRFilter(&bmp280, BMP280_FILTER_OFF);
 * bmp280ErrorState = bmp280_Set3WireSPI(&bmp280, BMP280_3WireSPI_OFF);
 * @endcode
 *
 * @param bmp BMP280 handler address pointer.
 * @param i2c I2C HAL handler address pointer.
 * @param address BMP280 sensor address.
 * @return Error state.
 *
 * @see bmp280_LinkDriver()
 * @see bmp280_UnLinkDriver()
 * @see bmp280_SetTemperatureOversampling()
 * @see bmp280_SetPressureOversampling()
 * @see bmp280_SetMode()
 * @see bmp280_GetMode()
 * @see bmp280_GetTemperatureOversampling()
 * @see bmp280_GetPressureOversampling()
 * @see BMP280_ReadTemperature()
 * @see BMP280_ReadPressureAndTemperature()
 */
bmp280_status_t  bmp280_init(bmp280_t *bmp, I2C_HandleTypeDef *i2c, uint8_t address);

/**
 * @ingroup BMP280
 * @brief Controls inactive duration time standby in normal mode
 *
 * @param bmp BMP280 handler address pointer.
 * @param StandbyTime BMP280 operating Standby Time.
 * @code
 * BMP280_STANDBY_MS_0_5
 * BMP280_STANDBY_MS_62_5
 * BMP280_STANDBY_MS_125
 * BMP280_STANDBY_MS_250
 * BMP280_STANDBY_MS_500
 * BMP280_STANDBY_MS_1000
 * BMP280_STANDBY_MS_2000
 * BMP280_STANDBY_MS_4000
 * @endcode
 *
 * @return Error state.
 */
bmp280_status_t bmp280_SetStandbyTime(bmp280_t *bmp, uint8_t StandbyTime);

/**
 * @ingroup BMP280
 * @brief Controls the time constant of the IIR filter.
 *
 * @param bmp BMP280 handler address pointer.
 * @param IIRFilter BMP280 variable IIR filter.
 * @code
 * 	BMP280_FILTER_OFF
 * 	BMP280_FILTER_X2
 * 	BMP280_FILTER_X4
 * 	BMP280_FILTER_X8
 * 	BMP280_FILTER_X16
 * @endcode
 *
 * @return Error state.
 */
bmp280_status_t bmp280_SetIIRFilter(bmp280_t *bmp, uint8_t IIRFilter); // todo

/**
 * @ingroup BMP280
 * @brief Enables 3-wire SPI interface when set to ‘1’.
 *
 * @param bmp BMP280 handler address pointer.
 * @param Enable3WireSPI BMP280 enaple 2-wire SPI .
 * @code
 * BMP280_3WireSPI_ON
 * BMP280_3WireSPI_OFF
 * @endcode
 *
 * @return Error state.
 */
bmp280_status_t bmp280_Set3WireSPI(bmp280_t *bmp, uint8_t Enable3WireSPI); // todo

/**
 * @ingroup BMP280
 * @brief Set Power modes. The BMP280 offers three power modes: sleep mode, forced mode and normal mode.
 * These can be selected using the mode[1:0] bits in control register 0xF4.
 *
 * @param bmp BMP280 handler address pointer.
 * @param Mode BMP280 operating modes.
 * @code
 * 	BMP280_SLEEPMODE
 * 	BMP280_FORCEDMODE
 * 	BMP280_NORMALMODE
 * @endcode
 *
 * @return Error state.
 */
bmp280_status_t bmp280_SetMode(bmp280_t *bmp, uint8_t Mode);

/**
 * @ingroup BMP280
 * @brief Controls oversampling of pressure data.
 *
 * @param bmp BMP280 handler address pointer.
 * @param PressureOversampling BMP280 pressure resolution.
 * @code
 * 	BMP280_DISABLE_PRESSURE
 * 	BMP280_ULTRALOWPOWER
 * 	BMP280_LOWPOWER
 * 	BMP280_STANDARD
 * 	BMP280_HIGHRES
 * 	BMP280_ULTRAHIGHRES
 * @endcode
 *
 * @return Error state.
 */
bmp280_status_t bmp280_SetPressureOversampling(bmp280_t *bmp, uint8_t PressureOversampling);

/**
 * @ingroup BMP280
 * @brief Controls oversampling of temperature data.
 *
 * @param bmp BMP280 handler address pointer.
 * @param TemperatureOversampling BMP280 temperature resolution.
 * @code
 * 	BMP280_DISABLE_TEMPERATURE
 * 	BMP280_TEMPERATURE_16BIT
 * 	BMP280_TEMPERATURE_17BIT
 * 	BMP280_TEMPERATURE_18BIT
 * 	BMP280_TEMPERATURE_19BIT
 * 	BMP280_TEMPERATURE_20BIT
 * @endcode
 *
 * @return Error state.
 */
bmp280_status_t bmp280_SetTemperatureOversampling(bmp280_t *bmp, uint8_t TemperatureOversampling);

/**
 * @ingroup BMP280
 * @brief Get saved standby time setting.
 *
 * @param bmp BMP280 handler address pointer.
 *
 * @param number of saved value.
 * @code
 * 0 - BMP280_STANDBY_MS_0_5
 * 1 - BMP280_STANDBY_MS_62_5
 * 2 - BMP280_STANDBY_MS_125
 * 3 - BMP280_STANDBY_MS_250
 * 4 - BMP280_STANDBY_MS_500
 * 5 - BMP280_STANDBY_MS_1000
 * 6 - BMP280_STANDBY_MS_2000
 * 7 - BMP280_STANDBY_MS_4000
 * @endcode
 */
uint8_t bmp280_GetStandbyTime(bmp280_t *bmp);

/**
 * @ingroup BMP280
 * @brief Get saved IIR filter setting.
 *
 * @param bmp BMP280 handler address pointer.
 *
 * @return number of saved value.
 * @code
 * 0 - BMP280_FILTER_OFF
 * 1	- BMP280_FILTER_X2
 * 2	- BMP280_FILTER_X4
 * 3	- BMP280_FILTER_X8
 * 4	- BMP280_FILTER_X16
 * @endcode
 */
uint8_t bmp280_GetIIRFilter(bmp280_t *bmp);

/**
 * @ingroup BMP280
 * @brief Get saved 3-Wire SPI setting.
 *
 * @param bmp BMP280 handler address pointer.
 *
 * @return number of saved value.
 * @code
 * 1 - BMP280_3WireSPI_ON
 * 0 - BMP280_3WireSPI_OFF
 * @endcode
 */
uint8_t bmp280_Get3WireSPI(bmp280_t *bmp);

/**
 * @ingroup BMP280
 * @brief Get saved Mode setting.
 *
 * @param bmp BMP280 handler address pointer.
 *
 * @return number of saved value.
 * @code
 * 0 - BMP280_SLEEPMODE
 * 1 or 2 - BMP280_FORCEDMODE
 * 3 - BMP280_NORMALMODE
 * @endcode
 */
uint8_t bmp280_GetMode(bmp280_t *bmp);

/**
 * @ingroup BMP280
 * @brief Get saved Temperature Oversampling setting.
 *
 * @param bmp BMP280 handler address pointer.
 *
 * @return number of saved value.
 * @code
 * 0 - BMP280_DISABLE_TEMPERATURE
 * 1 - BMP280_TEMPERATURE_16BIT
 * 2 - BMP280_TEMPERATURE_17BIT
 * 3 - BMP280_TEMPERATURE_18BIT
 * 4 - BMP280_TEMPERATURE_19BIT
 * 5 - BMP280_TEMPERATURE_20BIT
 * @endcode
 */
uint8_t bmp280_GetTemperatureOversampling(bmp280_t *bmp);

/**
 * @ingroup BMP280
 * @brief Get saved Pressure Oversampling setting.
 *
 * @param bmp BMP280 handler address pointer.
 *
 * @return number of saved value.
 * @code
 * 0 - BMP280_DISABLE_PRESSURE
 * 1 - BMP280_ULTRALOWPOWER
 * 2 - BMP280_LOWPOWER
 * 3 - BMP280_STANDARD
 * 4 - BMP280_HIGHRES
 * 5 - BMP280_ULTRAHIGHRES
 * @endcode
 */
uint8_t bmp280_GetPressureOversampling(bmp280_t *bmp);

/**
 * @ingroup BMP280
 * @brief Reading temperature.
 *
 * @param bmp BMP280 handler address pointer.
 *
 * @return Temperature measurement result.
 */
float BMP280_ReadTemperature(bmp280_t *bmp);

/**
 * @ingroup BMP280
 * @brief Reading temperature and pressure.
 *
 * @param bmp BMP280 handler address pointer.
 * @param Pressure Pointer of pressure measurement result.
 * @param Temperature Pointer of temperature measurement result.
 *
 * @return Error state.
 */
bmp280_status_t BMP280_ReadPressureAndTemperature(bmp280_t *bmp, float *Pressure, float *Temperature);
/**
 * @ingroup BMP280
 * @brief link driver.
 *
 * @param driver_bmp280 Pointer of driver.
 *
 * @return Error state.
 */
bmp280_status_t bmp280_LinkDriver(bmp280_driver_t *driver_bmp280);

/**
 * @ingroup BMP280
 * @brief Unlink driver.
 *
 * @return Error state.
 */
bmp280_status_t bmp280_UnLinkDriver(void);
#endif /* BMP280_H_ */

