/**
 * bmp280.c
 *
 * \ingroup BMP280
 *
 *  Created on: Apr 24, 2023
 *  @author Jan Łukaszewicz
 */

#include "bmp280.h"

static bmp280_driver_t *Bmp280Driver;

bmp280_status_t bmp280_LinkDriver(bmp280_driver_t *driver_bmp280)
{
	POINTER_GUARD_OF_DRIVER
	Bmp280Driver = driver_bmp280;

	return bmp280_NoError;
}

bmp280_status_t bmp280_UnLinkDriver(void)
{
		Bmp280Driver = NULL;
		return bmp280_NoError;
}

bmp280_status_t  bmp280_compensation_read(bmp280_t *bmp)
{
	uint8_t Value[24];

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read24Bytes)
	Bmp280Driver->Read24Bytes(bmp, BMP280_DIG_T1, Value);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead)

	bmp->t1 = ((Value[1] << 8) | Value[0]);
	bmp->t2 = ((Value[3] << 8) | Value[2]);
	bmp->t3 =  ((Value[5] << 8) | Value[4]);

	bmp->p1 =  ((Value[7] << 8) | Value[6]);
	bmp->p2 = ((Value[9] << 8) | Value[8]);
	bmp->p3 =  ((Value[11] << 8) | Value[10]);
	bmp->p4 =  ((Value[13] << 8) | Value[12]);
	bmp->p5 =  ((Value[15] << 8) | Value[14]);
	bmp->p6 =  ((Value[17] << 8) | Value[16]);
	bmp->p7 =  ((Value[19] << 8) | Value[18]);
	bmp->p8 =  ((Value[21] << 8) | Value[20]);
	bmp->p9 =  ((Value[23] << 8) | Value[22]);

	return bmp280_NoError;
}

bmp280_status_t bmp280_CheckCorrectSaveRegistry(bmp280_t *bmp, uint8_t bmp_register, uint8_t clearBits, uint8_t valueSetting)
{
	uint8_t Tmp;

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	Tmp = Bmp280Driver->Read8Bits(bmp, bmp_register);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorWriteToRegister)

	 CLEAR_BIT(Tmp, clearBits);

	if (Tmp == valueSetting)
	{
		return bmp280_NoError;
	}
	else
	{
		return bmp280_ErrorWriteToRegister;
	}
}

uint8_t bmp280_GetStandbyTime(bmp280_t *bmp)
{
	return bmp->settings.StandbyDuration;
}

uint8_t bmp280_GetIIRFilter(bmp280_t *bmp)
{
	return bmp->settings.IIRFilter;
}

uint8_t bmp280_Get3WireSPI(bmp280_t *bmp)
{
	return bmp->settings.IIRFilter;
}

uint8_t bmp280_GetMode(bmp280_t *bmp)
{
	return bmp->settings.Mode;
}

uint8_t bmp280_GetTemperatureOversampling(bmp280_t *bmp)
{
	return bmp->settings.Temerature;
}

uint8_t bmp280_GetPressureOversampling(bmp280_t *bmp)
{
	return bmp->settings.Pressure;
}

bmp280_status_t bmp280_SetStandbyTime(bmp280_t *bmp, uint8_t StandbyTime)
{
	uint8_t Tmp;

	if(StandbyTime > BMP280_MAX_STANDBY)
	{
		StandbyTime = BMP280_STANDBY_MS_4000;
	}

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	Tmp = Bmp280Driver->Read8Bits(bmp, BMP280_CONFIG);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead) // checking errors in Read8Bits callback function

	CLEAR_BIT(Tmp,0xE0);					// 1110 0000
	SET_BIT(Tmp, (StandbyTime << 5));
//	Tmp = Tmp & 0x1F; 					// 0001 1111 delete bits
//	Tmp |= ((StandbyTime << 5) & 0xE0);	// 1110 0000 set bits

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Write8Bits)
	Bmp280Driver->Write8Bits(bmp, BMP280_CONFIG, Tmp);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorWrite) // checking errors in Write8Bits callback function

	bmp->ErrorState = bmp280_CheckCorrectSaveRegistry(bmp, BMP280_CONFIG,  0x1F, (StandbyTime << 5) );

	if (bmp280_NoError == (bmp->ErrorState))
	{
		bmp->settings.StandbyDuration = StandbyTime;
	}
	else
	{
		return (bmp->ErrorState);
	}

	return bmp280_NoError;
}

bmp280_status_t bmp280_SetIIRFilter(bmp280_t *bmp, uint8_t IIRFilter)
{
	uint8_t Tmp;

	if(IIRFilter > BMP280_MAX_IIRFILTER)
	{
		IIRFilter = BMP280_FILTER_X16;
	}

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	Tmp = Bmp280Driver->Read8Bits(bmp, BMP280_CONFIG);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead) // checking errors in Read8Bits callback function

	CLEAR_BIT(Tmp, 0x1C);			// 0001 1100
	SET_BIT(Tmp, (IIRFilter << 2));
//	Tmp = Tmp & 0xE3; 					 			// 1110 0011 delete bits
//	Tmp |= ((IIRFilter << 2) & 0x1C);					// 0001 1100 set bits. first is delete bits and second is set bits

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Write8Bits)
	Bmp280Driver->Write8Bits(bmp, BMP280_CONFIG, Tmp);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorWrite) // checking errors in Write8Bits callback function

	bmp->ErrorState = bmp280_CheckCorrectSaveRegistry(bmp, BMP280_CONFIG, (~0x1C), (IIRFilter << 2) );

	if (bmp280_NoError == (bmp->ErrorState))
	{
		bmp->settings.IIRFilter = IIRFilter;
	}
	else
	{
		return (bmp->ErrorState);
	}
	return bmp280_NoError;
}

bmp280_status_t bmp280_Set3WireSPI(bmp280_t *bmp, uint8_t Enable3WireSPI)
{
	uint8_t Tmp;

	if(Enable3WireSPI > BMP280_MAX_3WireSPI)
	{
		Enable3WireSPI = BMP280_MAX_3WireSPI;
	}

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	Tmp = Bmp280Driver->Read8Bits(bmp, BMP280_CONFIG);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead) // checking errors in Read8Bits callback function

	CLEAR_BIT(Tmp, 0x01);			// 0000 0001 set bits.
	SET_BIT(Tmp,Enable3WireSPI);

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Write8Bits)
	Bmp280Driver->Write8Bits(bmp, BMP280_CONFIG, Tmp);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorWrite) // checking errors in Write8Bits callback function

	bmp->ErrorState = bmp280_CheckCorrectSaveRegistry(bmp, BMP280_CONFIG, (~0x01), Enable3WireSPI );

	if (bmp280_NoError == (bmp->ErrorState))
	{
		bmp->settings.Spi3wireEnable = Enable3WireSPI;
	}
	else
	{
		return (bmp->ErrorState);
	}
	return bmp280_NoError;
}


bmp280_status_t bmp280_SetMode(bmp280_t *bmp, uint8_t Mode)
{
	uint8_t Tmp;

	if(Mode > BMP280_MAX_REG_VALUE_Mode)
	{
		Mode = BMP280_NORMALMODE;
	}

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	Tmp = Bmp280Driver->Read8Bits(bmp, BMP280_CONTROL);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead)

	CLEAR_BIT(Tmp, 0x03);	 // 0000 0011
	SET_BIT(Tmp, Mode);
//	Tmp = Tmp & 0xFC;  	// xxxx xx00
//	Tmp |= Mode & 0x03;  // 0000 0011

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Write8Bits)
	Bmp280Driver->Write8Bits(bmp, BMP280_CONTROL, Tmp);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorWrite)

	bmp->ErrorState = bmp280_CheckCorrectSaveRegistry(bmp, BMP280_CONTROL, (~0x03), Mode);

	if(bmp280_NoError == (bmp->ErrorState))
	{
		bmp->settings.Mode = Mode;
	}
	else
	{
		return (bmp->ErrorState);
	}

	return bmp280_NoError;
}

bmp280_status_t bmp280_SetTemperatureOversampling(bmp280_t *bmp, uint8_t TemperatureOversampling)
{
	uint8_t Tmp;

	if(TemperatureOversampling > BMP280_MAX_REG_VALUE_TEMPERATURE)
	{
		TemperatureOversampling = BMP280_TEMPERATURE_20BIT;
	}

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	Tmp = Bmp280Driver->Read8Bits(bmp, BMP280_CONTROL);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead) // checking errors in Read8Bits callback function

	CLEAR_BIT(Tmp, 0xE0);  // 1110 0000
	SET_BIT(Tmp, (TemperatureOversampling << 5));
//	Tmp = Tmp & 0x1F; 					 			// 000x xxxx
//	Tmp |= ((TemperatureOversampling << 5) & 0xE0);	// 1110 0000

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Write8Bits)
	Bmp280Driver->Write8Bits(bmp, BMP280_CONTROL, Tmp);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorWrite) // checking errors in Write8Bits callback function

	bmp->ErrorState = bmp280_CheckCorrectSaveRegistry(bmp, BMP280_CONTROL, 0x1F, (TemperatureOversampling << 5) );

	if (bmp280_NoError == (bmp->ErrorState))
	{
		bmp->settings.Temerature = TemperatureOversampling;
	}
	else
	{
		return (bmp->ErrorState);
	}

	return bmp280_NoError;
}

bmp280_status_t bmp280_SetPressureOversampling(bmp280_t *bmp, uint8_t PressureOversampling)
{
	uint8_t Tmp;

	if(PressureOversampling > BMP280_MAX_REG_VALUE_PRESSURE)
	{
		PressureOversampling = BMP280_ULTRAHIGHRES;
	}

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	Tmp = Bmp280Driver->Read8Bits(bmp, BMP280_CONTROL);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead)

	CLEAR_BIT(Tmp, 0x1C);							//xxx1 11xx
	SET_BIT(Tmp, (PressureOversampling << 2));
//	Tmp = Tmp & 0xE3; 					 		// xxx0 00xx
//	Tmp |= ((PressureOversampling << 2) & 0x1C);	// 0001 1100


	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Write8Bits)
	Bmp280Driver->Write8Bits(bmp, BMP280_CONTROL, Tmp);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorWrite)

	bmp->ErrorState = bmp280_CheckCorrectSaveRegistry(bmp, BMP280_CONTROL, 0xE3, (PressureOversampling << 2));

	if (bmp280_NoError == (bmp->ErrorState))
	{
		bmp->settings.Pressure = PressureOversampling;
	}
	else
	{
		return (bmp->ErrorState);
	}

	return bmp280_NoError;
}

uint32_t BMP280_ReadTemperatureRaw(bmp280_t *bmp)
{
	int32_t Tmp=0;
	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read24Bits)
	Tmp = (int32_t)Bmp280Driver->Read24Bits(bmp, BMP280_TEMPDATA); // we get shifted value FF FF F0

	Tmp >>= 4; // 0F FF FF

	return Tmp;
}

uint32_t BMP280_ReadPressureRaw(bmp280_t *bmp)
{
	int32_t Tmp;
	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read24Bits)
	Tmp = (int32_t)Bmp280Driver->Read24Bits(bmp, BMP280_PRESSUREDATA); // we get shifted value FF FF F0

	Tmp >>= 4; // 0F FF FF

	return Tmp;
}

float BMP280_ReadTemperature(bmp280_t *bmp)
{
	int32_t var1, var2, T, adc_T;

	adc_T = BMP280_ReadTemperatureRaw(bmp);

	var1 = ((((adc_T>>3) - ((int32_t)(bmp->t1)<<1))) * ((int32_t)(bmp->t2))) >> 11;

	var2 = (((((adc_T>>4) - ((int32_t)(bmp->t1))) * ((adc_T>>4) - ((int32_t)(bmp->t1)))) >> 12) *
	((int32_t)(bmp->t3))) >> 14;

	bmp->t_fine = var1 + var2;

	T = ((bmp->t_fine) * 5 + 128) >> 8;

	return (float)(T/100.0);
}

float BMP280_ReadPressure(bmp280_t *bmp)
{
	int32_t var1, var2, adc_P;
	uint32_t p;

	adc_P = BMP280_ReadPressureRaw(bmp);

	var1 = (((int32_t)(bmp->t_fine))>>1) - (int32_t)64000;

	var2 = (((var1>>2) * (var1>>2)) >> 11 ) * ((int32_t)(bmp->p6));
	var2 = var2 + ((var1*((int32_t)(bmp->p5)))<<1);
	var2 = (var2>>2)+(((int32_t)(bmp->p4))<<16);

	var1 = ((((bmp->p3) * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + ((((int32_t)(bmp->p2)) * var1)>>1))>>18;
	var1 =((((32768+var1))*((int32_t)(bmp->p1)))>>15);

	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}

	p = (((uint32_t)(((int32_t)1048576)-adc_P)-(var2>>12)))*3125;

	if (p < 0x80000000)
	{
		p = (p << 1) / ((uint32_t)var1);
	}
	else
	{
		p = (p / (uint32_t)var1) * 2;
	}

	var1 = (((int32_t)(bmp->p9)) * ((int32_t)(((p>>3) * (p>>3))>>13)))>>12;

	var2 = (((int32_t)(p>>2)) * ((int32_t)(bmp->p8)))>>13;

	p = (uint32_t)((int32_t)p + ((var1 + var2 + (bmp->p7)) >> 4));

//	p = (float)(p/100.0);
	return  (float)(p/100.0);
}

bmp280_status_t BMP280_ReadPressureAndTemperature(bmp280_t *bmp, float *Pressure, float *Temperature)
{
	*Temperature = BMP280_ReadTemperature(bmp);

	*Pressure = BMP280_ReadPressure(bmp);

	return bmp280_NoError;
}

bmp280_status_t  bmp280_init(bmp280_t *bmp, I2C_HandleTypeDef *i2c, uint8_t Address_i2c)
{
	uint8_t chipID;

	bmp280_status_t ExecutionSetState[3];

	bmp->bmp_i2c =  i2c;
	bmp->address_i2c = Address_i2c;

	POINTER_GUARD_OF_DRIVER_CALLBACK(Bmp280Driver->Read8Bits)
	chipID = Bmp280Driver->Read8Bits(bmp, BMP280_CHIPID);
	ERROR_CHECK(bmp->ErrorState, bmp280_ErrorRead)

	if(chipID != 0x58)
	{
		return bmp280_ErrorChipID;
	}

	bmp280_compensation_read(bmp); // burst reading timing 2.6ms

	ExecutionSetState[0] = bmp280_SetTemperatureOversampling(bmp, BMP280_TEMPERATURE_19BIT);
	ExecutionSetState[1] = bmp280_SetPressureOversampling(bmp, BMP280_ULTRAHIGHRES);
	ExecutionSetState[2] = bmp280_SetMode(bmp, BMP280_NORMALMODE);
	for (uint8_t i = 0; i < 3; i++)
	{
		if(bmp280_NoError < ExecutionSetState[i])
		{
			return bmp280_ErrorWriteToRegister;
		}
	}
	return bmp280_NoError;
}
