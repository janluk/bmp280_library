/*
 * bmp280_driver.h
 *
 *  author Jan Łukaszewicz
 *  date 2023.05.10
 */

/**
 * \ingroup DRIVER
 * @{
 */

#ifndef BMP280_DRIVER_H_
#define BMP280_DRIVER_H_

#include "bmp280.h"

typedef struct
{
	/**
	 * Reads 8 bits from register address
	 *
	 * @param *bmp BMP280 strcture
	 * @param ReadFromRegisterAddress Addres of BMP280 register to read
	 * @return Returns read one byte from the register
	 */
	uint8_t (*Read8Bits)(bmp280_t *bmp280, uint8_t ReadFromRegisterAddress);

	/**
	 * Writes 8 bits to indicated register
	 *
	 * @param *bmp BMP280 strcture.
	 * @param WriteToRegister Addres of BMP280 register to write
	 * @param ValueForRegister Data to writes to the register
	 */
	void (*Write8Bits)(bmp280_t *bmp280, uint8_t WriteToRegisterAddress, uint8_t ValueForRegister);

	/**
	 * Reads 24 bits from register address
	 *
	 * @param *bmp BMP280 strcture
	 * @param ReadFromRegisterAddress Addres of BMP280 register to read
	 * @return Returns read 24 bits from the register
	 *
	 * @note returned value should by:
	 * \code{.c}
	 *
	 * | FF		| FF 	| F0 	|
	 * | MSB	| LSB 	| xLSB 	|
	 *
	  * return ((Value[0] << 16) | (Value[1] << 8) | Value[2]);
	 * \endcode
	 */
	uint32_t (*Read24Bits)(bmp280_t *bmp280, uint8_t ReadFromRegisterAddress);

	/**
	 * Reads 24 bits from register address
	 *
	 * @param *bmp BMP280 strcture
	 * @param ReadFromRegisterAddress Addres of BMP280 register to read
	 * @param  Write[24] Returns the 24 bytes read from the register to the indicated 24-element array using a pointer
	 */
	void (*Read24Bytes)(bmp280_t *bmp280, uint8_t ReadFromRegisterAddress, uint8_t WriteData[24]);

}bmp280_driver_t;

#endif /* BMP280_DRIVER_H_ */
/// @}
