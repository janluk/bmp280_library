var structbmp280__settings__t =
[
    [ "IIRFilter", "group___d_e_f___g_u_a_r_d.html#gae0582a30c7dd5d5aa5265698c5cdb376", null ],
    [ "Mode", "group___d_e_f___g_u_a_r_d.html#gaa03324888d9a024fa1af3c538d3c0f77", null ],
    [ "Pressure", "group___d_e_f___g_u_a_r_d.html#ga38be97880a1dbc25eed84de9883e5717", null ],
    [ "Spi3wireEnable", "group___d_e_f___g_u_a_r_d.html#ga152e9496f5cec34ac97c9b60b7709a09", null ],
    [ "StandbyDuration", "group___d_e_f___g_u_a_r_d.html#ga3853b9a619a7ea31c3e59cc9552dc2f1", null ],
    [ "Temerature", "group___d_e_f___g_u_a_r_d.html#ga5cc15722733eac76a92f7185a89951b2", null ]
];