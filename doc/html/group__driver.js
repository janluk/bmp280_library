var group__driver =
[
    [ "bmp280_driver_t", "structbmp280__driver__t.html", [
      [ "Read24Bits", "group__driver.html#gaabec676abccc5be0de6b35ea68b70a29", null ],
      [ "Read24Bytes", "group__driver.html#gaa8cf5193005a55dfacc30d085eded4c8", null ],
      [ "Read8Bits", "group__driver.html#gaf08dd03ffc83f1571a4a6c950cf4f84b", null ],
      [ "Write8Bits", "group__driver.html#ga38af6d683aa7592ba9271d8261442217", null ]
    ] ],
    [ "bmp280_driver_t::Read24Bits", "group__driver.html#gaabec676abccc5be0de6b35ea68b70a29", null ],
    [ "bmp280_driver_t::Read24Bytes", "group__driver.html#gaa8cf5193005a55dfacc30d085eded4c8", null ],
    [ "bmp280_driver_t::Read8Bits", "group__driver.html#gaf08dd03ffc83f1571a4a6c950cf4f84b", null ],
    [ "bmp280_driver_t::Write8Bits", "group__driver.html#ga38af6d683aa7592ba9271d8261442217", null ]
];