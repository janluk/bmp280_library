var group___d_r_i_v_e_r =
[
    [ "bmp280_driver_t", "structbmp280__driver__t.html", [
      [ "Read24Bits", "structbmp280__driver__t.html#aabec676abccc5be0de6b35ea68b70a29", null ],
      [ "Read24Bytes", "structbmp280__driver__t.html#a84ef4362f03a957b967d528272a643ad", null ],
      [ "Read8Bits", "structbmp280__driver__t.html#af08dd03ffc83f1571a4a6c950cf4f84b", null ],
      [ "Write8Bits", "structbmp280__driver__t.html#a38af6d683aa7592ba9271d8261442217", null ]
    ] ]
];