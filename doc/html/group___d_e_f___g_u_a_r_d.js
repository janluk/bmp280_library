var group___d_e_f___g_u_a_r_d =
[
    [ "Operation on bits", "group___d_e_f___b_i_t_s.html", "group___d_e_f___b_i_t_s" ],
    [ "Temperature resolution", "group___d_e_f___t_e_m_p.html", "group___d_e_f___t_e_m_p" ],
    [ "Pressure oversampling", "group___d_e_f___p_r_e_s_s_u_r_e.html", "group___d_e_f___p_r_e_s_s_u_r_e" ],
    [ "Standby time", "group___d_e_f___s_t_a_n_d_b_y.html", "group___d_e_f___s_t_a_n_d_b_y" ],
    [ "IIR filter", "group___d_e_f___i_i_r.html", "group___d_e_f___i_i_r" ],
    [ "3-Wire SPI", "group___d_e_f__3_wire_s_p_i.html", "group___d_e_f__3_wire_s_p_i" ],
    [ "Mode", "group___d_e_f___m_o_d_e.html", "group___d_e_f___m_o_d_e" ],
    [ "Coeffs registers", "group___d_e_f___c_o_e_f_f_s.html", "group___d_e_f___c_o_e_f_f_s" ],
    [ "register addresses", "group___d_e_f___r_e_g___a_d_d_r.html", "group___d_e_f___r_e_g___a_d_d_r" ],
    [ "bmp280_settings_t", "structbmp280__settings__t.html", [
      [ "IIRFilter", "group___d_e_f___g_u_a_r_d.html#gae0582a30c7dd5d5aa5265698c5cdb376", null ],
      [ "Mode", "group___d_e_f___g_u_a_r_d.html#gaa03324888d9a024fa1af3c538d3c0f77", null ],
      [ "Pressure", "group___d_e_f___g_u_a_r_d.html#ga38be97880a1dbc25eed84de9883e5717", null ],
      [ "Spi3wireEnable", "group___d_e_f___g_u_a_r_d.html#ga152e9496f5cec34ac97c9b60b7709a09", null ],
      [ "StandbyDuration", "group___d_e_f___g_u_a_r_d.html#ga3853b9a619a7ea31c3e59cc9552dc2f1", null ],
      [ "Temerature", "group___d_e_f___g_u_a_r_d.html#ga5cc15722733eac76a92f7185a89951b2", null ]
    ] ],
    [ "bmp280_t", "structbmp280__t.html", [
      [ "address_i2c", "group___d_e_f___g_u_a_r_d.html#gad0686a999ace1f30f28a293513830c4c", null ],
      [ "bmp_i2c", "group___d_e_f___g_u_a_r_d.html#gae1feec127ef5e178b79bdf86c7204f98", null ],
      [ "ErrorState", "group___d_e_f___g_u_a_r_d.html#ga9c64a3d7c2d76a47c9f9fb330b38ce7f", null ],
      [ "p1", "group___d_e_f___g_u_a_r_d.html#ga7aeb9e3a704d61d8b8b10c1cbf3364de", null ],
      [ "p2", "group___d_e_f___g_u_a_r_d.html#gaa629d5d644a7f3b71051d49179ab8257", null ],
      [ "p3", "group___d_e_f___g_u_a_r_d.html#gad5735dba87c2403296db23326d282b83", null ],
      [ "p4", "group___d_e_f___g_u_a_r_d.html#gabcc4c5424fd683e40fd1fdfcb2ee9209", null ],
      [ "p5", "group___d_e_f___g_u_a_r_d.html#ga9f437057c7521a8b526851bb93f27e48", null ],
      [ "p6", "group___d_e_f___g_u_a_r_d.html#gacc714399e369aae0bcbd9ce01c3879a0", null ],
      [ "p7", "group___d_e_f___g_u_a_r_d.html#gad2b1a3073ff5b2d937f4fa76efc10028", null ],
      [ "p8", "group___d_e_f___g_u_a_r_d.html#ga08157220e358821a812047cb359605df", null ],
      [ "p9", "group___d_e_f___g_u_a_r_d.html#ga868d9a7ca86584941fc36f5c95f0858e", null ],
      [ "settings", "group___d_e_f___g_u_a_r_d.html#gabb3c9d3772a3c7f1085692aae78f5565", null ],
      [ "t1", "group___d_e_f___g_u_a_r_d.html#gae483369f6a34f099a733f98723170d26", null ],
      [ "t2", "group___d_e_f___g_u_a_r_d.html#gae820124af8d9c12076600484931b3f47", null ],
      [ "t3", "group___d_e_f___g_u_a_r_d.html#ga2f730bbb78cad91ca567b495ba54bc46", null ],
      [ "t_fine", "group___d_e_f___g_u_a_r_d.html#ga95cd37a3f5de8fbeecd1c5b0fc877582", null ]
    ] ],
    [ "ERROR_CHECK", "group___d_e_f___g_u_a_r_d.html#gaf6b0084cdc8ef28b4be57d6d251b0c24", null ],
    [ "POINTER_GUARD_OF_DRIVER", "group___d_e_f___g_u_a_r_d.html#gae397b74a93cdfca0f517a3f72158bc91", null ],
    [ "POINTER_GUARD_OF_DRIVER_CALLBACK", "group___d_e_f___g_u_a_r_d.html#gae7bfad390c9a9d57b139074830ddbce8", null ],
    [ "bmp280_ErrorCallbackPointer", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4a98ee066f7808938b552f8044aa95dd88", null ],
    [ "bmp280_ErrorChipID", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4af1b9ae797da8defc80ba67114b55d442", null ],
    [ "bmp280_ErrorLinkDriver", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4a3cae9b4ef18b5f921f1d154360e5e82f", null ],
    [ "bmp280_ErrorRead", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4a8eb527786fbbd14c2102d490ad39f305", null ],
    [ "bmp280_ErrorWrite", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4a419228c1633df14c41cce4797d62343d", null ],
    [ "bmp280_ErrorWriteToRegister", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4ae440575697c841f7391f0a12f39490e8", null ],
    [ "bmp280_HAL_BUSY", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4a94511966ffedbc7295353521fbcc7854", null ],
    [ "bmp280_HAL_ERROR", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4aedc338204e334fc592d4ed5343360dd0", null ],
    [ "bmp280_HAL_TIMEOUT", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4a6e78e7b83ae6c4e113c7b69e63728d89", null ],
    [ "bmp280_NoError", "group___b_m_p280.html#gga6e1ca14b0a3292d9f3955fe82d8ceef4a3840a24ed91a53424816fde1056eba67", null ],
    [ "bmp280_t::address_i2c", "group___d_e_f___g_u_a_r_d.html#gad0686a999ace1f30f28a293513830c4c", null ],
    [ "bmp280_t::bmp_i2c", "group___d_e_f___g_u_a_r_d.html#gae1feec127ef5e178b79bdf86c7204f98", null ],
    [ "bmp280_t::ErrorState", "group___d_e_f___g_u_a_r_d.html#ga9c64a3d7c2d76a47c9f9fb330b38ce7f", null ],
    [ "bmp280_settings_t::IIRFilter", "group___d_e_f___g_u_a_r_d.html#gae0582a30c7dd5d5aa5265698c5cdb376", null ],
    [ "bmp280_settings_t::Mode", "group___d_e_f___g_u_a_r_d.html#gaa03324888d9a024fa1af3c538d3c0f77", null ],
    [ "bmp280_t::p1", "group___d_e_f___g_u_a_r_d.html#ga7aeb9e3a704d61d8b8b10c1cbf3364de", null ],
    [ "bmp280_t::p2", "group___d_e_f___g_u_a_r_d.html#gaa629d5d644a7f3b71051d49179ab8257", null ],
    [ "bmp280_t::p3", "group___d_e_f___g_u_a_r_d.html#gad5735dba87c2403296db23326d282b83", null ],
    [ "bmp280_t::p4", "group___d_e_f___g_u_a_r_d.html#gabcc4c5424fd683e40fd1fdfcb2ee9209", null ],
    [ "bmp280_t::p5", "group___d_e_f___g_u_a_r_d.html#ga9f437057c7521a8b526851bb93f27e48", null ],
    [ "bmp280_t::p6", "group___d_e_f___g_u_a_r_d.html#gacc714399e369aae0bcbd9ce01c3879a0", null ],
    [ "bmp280_t::p7", "group___d_e_f___g_u_a_r_d.html#gad2b1a3073ff5b2d937f4fa76efc10028", null ],
    [ "bmp280_t::p8", "group___d_e_f___g_u_a_r_d.html#ga08157220e358821a812047cb359605df", null ],
    [ "bmp280_t::p9", "group___d_e_f___g_u_a_r_d.html#ga868d9a7ca86584941fc36f5c95f0858e", null ],
    [ "bmp280_settings_t::Pressure", "group___d_e_f___g_u_a_r_d.html#ga38be97880a1dbc25eed84de9883e5717", null ],
    [ "bmp280_t::settings", "group___d_e_f___g_u_a_r_d.html#gabb3c9d3772a3c7f1085692aae78f5565", null ],
    [ "bmp280_settings_t::Spi3wireEnable", "group___d_e_f___g_u_a_r_d.html#ga152e9496f5cec34ac97c9b60b7709a09", null ],
    [ "bmp280_settings_t::StandbyDuration", "group___d_e_f___g_u_a_r_d.html#ga3853b9a619a7ea31c3e59cc9552dc2f1", null ],
    [ "bmp280_t::t1", "group___d_e_f___g_u_a_r_d.html#gae483369f6a34f099a733f98723170d26", null ],
    [ "bmp280_t::t2", "group___d_e_f___g_u_a_r_d.html#gae820124af8d9c12076600484931b3f47", null ],
    [ "bmp280_t::t3", "group___d_e_f___g_u_a_r_d.html#ga2f730bbb78cad91ca567b495ba54bc46", null ],
    [ "bmp280_t::t_fine", "group___d_e_f___g_u_a_r_d.html#ga95cd37a3f5de8fbeecd1c5b0fc877582", null ],
    [ "bmp280_settings_t::Temerature", "group___d_e_f___g_u_a_r_d.html#ga5cc15722733eac76a92f7185a89951b2", null ]
];