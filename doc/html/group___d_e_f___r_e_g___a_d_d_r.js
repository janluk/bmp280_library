var group___d_e_f___r_e_g___a_d_d_r =
[
    [ "BMP280_CAL26", "group___d_e_f___r_e_g___a_d_d_r.html#ga9542f7f255ff7320dc8bfc1268c30626", null ],
    [ "BMP280_CHIPID", "group___d_e_f___r_e_g___a_d_d_r.html#ga9c9df54d23b6b41855b1dbef4486b994", null ],
    [ "BMP280_CONFIG", "group___d_e_f___r_e_g___a_d_d_r.html#gaff44c962485877cd7e135bd15abd1267", null ],
    [ "BMP280_CONTROL", "group___d_e_f___r_e_g___a_d_d_r.html#ga6788a2d4fa82ffa35b54879142c9c96e", null ],
    [ "BMP280_PRESSUREDATA", "group___d_e_f___r_e_g___a_d_d_r.html#gac2fa6456576f421c2ddd4f8d4c251f3d", null ],
    [ "BMP280_SOFTRESET", "group___d_e_f___r_e_g___a_d_d_r.html#ga4468b4298eb89ae6dd3033540ae2e348", null ],
    [ "BMP280_STATUS", "group___d_e_f___r_e_g___a_d_d_r.html#ga77ec406644190fad9eec978e05d22606", null ],
    [ "BMP280_TEMPDATA", "group___d_e_f___r_e_g___a_d_d_r.html#ga5f3ec4399a9875bc9bd9ae5aa91831b3", null ],
    [ "BMP280_VERSION", "group___d_e_f___r_e_g___a_d_d_r.html#ga3eb22112cfbb81fb34321b18c7763fa9", null ]
];