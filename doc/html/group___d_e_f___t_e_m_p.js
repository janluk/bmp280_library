var group___d_e_f___t_e_m_p =
[
    [ "BMP280_DISABLE_TEMPERATURE", "group___d_e_f___t_e_m_p.html#ga9da3a1041465af384ac3155d64fe0017", null ],
    [ "BMP280_MAX_REG_VALUE_TEMPERATURE", "group___d_e_f___t_e_m_p.html#ga57bc8817fdef47e87d1e66d7417c6f7d", null ],
    [ "BMP280_TEMPERATURE_16BIT", "group___d_e_f___t_e_m_p.html#ga98846d729898f753061308f684c86cd0", null ],
    [ "BMP280_TEMPERATURE_17BIT", "group___d_e_f___t_e_m_p.html#ga0177b6886f19d60e820bdbf9aa79ea53", null ],
    [ "BMP280_TEMPERATURE_18BIT", "group___d_e_f___t_e_m_p.html#ga07cf8a404e1213a6a4539006ff8cf423", null ],
    [ "BMP280_TEMPERATURE_19BIT", "group___d_e_f___t_e_m_p.html#ga0e93c1f9e147ede3a05843a68d7e2cd0", null ],
    [ "BMP280_TEMPERATURE_20BIT", "group___d_e_f___t_e_m_p.html#gabfd36172b38f373f7471814d9ca4dca2", null ]
];