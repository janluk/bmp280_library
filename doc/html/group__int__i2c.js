var group__int__i2c =
[
    [ "bmp280_GetDriver", "group__int__i2c.html#gaea3bd9ac90f82a7fdabf0611dbc5d15b", null ],
    [ "bmp280Read24Bits", "group__int__i2c.html#ga9a2d845f2bc9f2cfec8d750097232760", null ],
    [ "bmp280Read24Bytes", "group__int__i2c.html#ga0fe9f50c09934fac6077103b67776a01", null ],
    [ "bmp280Read8", "group__int__i2c.html#ga0a2c9fe8898005db412a6ea3c40f5a79", null ],
    [ "bmp280Write8", "group__int__i2c.html#gaebd990923e2c747c54473406c455a4df", null ]
];