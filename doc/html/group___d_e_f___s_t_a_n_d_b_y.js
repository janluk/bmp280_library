var group___d_e_f___s_t_a_n_d_b_y =
[
    [ "BMP280_MAX_STANDBY", "group___d_e_f___s_t_a_n_d_b_y.html#gaa8f010d846501ac79c3726323dd56243", null ],
    [ "BMP280_STANDBY_MS_0_5", "group___d_e_f___s_t_a_n_d_b_y.html#gaba1a18a720df5f34104c760a8ff7034d", null ],
    [ "BMP280_STANDBY_MS_1000", "group___d_e_f___s_t_a_n_d_b_y.html#ga83a5973473a915b802c1367e0a209988", null ],
    [ "BMP280_STANDBY_MS_125", "group___d_e_f___s_t_a_n_d_b_y.html#ga46de09873bc6dc64718407fc91591ccd", null ],
    [ "BMP280_STANDBY_MS_2000", "group___d_e_f___s_t_a_n_d_b_y.html#ga32fac4752be07ffc3a8c4822b28a6122", null ],
    [ "BMP280_STANDBY_MS_250", "group___d_e_f___s_t_a_n_d_b_y.html#gacde4eb718cde1516f7fd8496f55568be", null ],
    [ "BMP280_STANDBY_MS_4000", "group___d_e_f___s_t_a_n_d_b_y.html#gafc96afab5cd18755c4e156425108308c", null ],
    [ "BMP280_STANDBY_MS_500", "group___d_e_f___s_t_a_n_d_b_y.html#ga127c7478cb7435bd264993a3f8a67ce4", null ],
    [ "BMP280_STANDBY_MS_62_5", "group___d_e_f___s_t_a_n_d_b_y.html#gabb3ab08f84b56340d01ca47927087f93", null ]
];