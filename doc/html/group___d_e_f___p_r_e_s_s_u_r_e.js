var group___d_e_f___p_r_e_s_s_u_r_e =
[
    [ "BMP280_DISABLE_PRESSURE", "group___d_e_f___p_r_e_s_s_u_r_e.html#ga567d09d8c2b24e99d7dbd232b857e418", null ],
    [ "BMP280_HIGHRES", "group___d_e_f___p_r_e_s_s_u_r_e.html#gab7b8dca562eab2205a43c9f69e7c53bc", null ],
    [ "BMP280_LOWPOWER", "group___d_e_f___p_r_e_s_s_u_r_e.html#ga80df7e580b463f421e699cb3190fd9d0", null ],
    [ "BMP280_MAX_REG_VALUE_PRESSURE", "group___d_e_f___p_r_e_s_s_u_r_e.html#ga7bfdf23614ed3db982caaa742f42fbcc", null ],
    [ "BMP280_STANDARD", "group___d_e_f___p_r_e_s_s_u_r_e.html#gacd0c0c2c0abdc4a32fd3db235e42d27a", null ],
    [ "BMP280_ULTRAHIGHRES", "group___d_e_f___p_r_e_s_s_u_r_e.html#gaf288680eb0834987006f856274653fda", null ],
    [ "BMP280_ULTRALOWPOWER", "group___d_e_f___p_r_e_s_s_u_r_e.html#gaf73be229f713df9af41d0e5e24e2f023", null ]
];