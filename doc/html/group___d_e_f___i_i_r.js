var group___d_e_f___i_i_r =
[
    [ "BMP280_FILTER_OFF", "group___d_e_f___i_i_r.html#ga788525a760c9cba4df301185908eca97", null ],
    [ "BMP280_FILTER_X16", "group___d_e_f___i_i_r.html#gaac50958372cd56a5fe8df0cf68b37ce9", null ],
    [ "BMP280_FILTER_X2", "group___d_e_f___i_i_r.html#ga66343baf9f54b1ae549682fe99a2846f", null ],
    [ "BMP280_FILTER_X4", "group___d_e_f___i_i_r.html#ga9ebb351663d61d645cb002ed68bd4763", null ],
    [ "BMP280_FILTER_X8", "group___d_e_f___i_i_r.html#gaf8660409b839ed48a7fbb287c2ac7139", null ],
    [ "BMP280_MAX_IIRFILTER", "group___d_e_f___i_i_r.html#ga66fa915265b600b83c0df4eb59eab0ba", null ]
];