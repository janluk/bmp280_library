var searchData=
[
  ['scanner_0',['I2C Scanner',['../group___s_c_a_n.html',1,'']]],
  ['set_5fbit_1',['SET_BIT',['../group___d_e_f___b_i_t_s.html#ga26474f43799fbade9cf300e21dd3a91a',1,'bmp280.h']]],
  ['settings_2',['settings',['../group___d_e_f___g_u_a_r_d.html#gabb3c9d3772a3c7f1085692aae78f5565',1,'bmp280_t']]],
  ['spi_3',['3-Wire SPI',['../group___d_e_f__3_wire_s_p_i.html',1,'']]],
  ['spi3wireenable_4',['Spi3wireEnable',['../group___d_e_f___g_u_a_r_d.html#ga152e9496f5cec34ac97c9b60b7709a09',1,'bmp280_settings_t']]],
  ['standby_20time_5',['Standby time',['../group___d_e_f___s_t_a_n_d_b_y.html',1,'']]],
  ['standbyduration_6',['StandbyDuration',['../group___d_e_f___g_u_a_r_d.html#ga3853b9a619a7ea31c3e59cc9552dc2f1',1,'bmp280_settings_t']]]
];
