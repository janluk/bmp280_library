var searchData=
[
  ['read24bits_0',['Read24Bits',['../structbmp280__driver__t.html#aabec676abccc5be0de6b35ea68b70a29',1,'bmp280_driver_t']]],
  ['read24bytes_1',['Read24Bytes',['../structbmp280__driver__t.html#a84ef4362f03a957b967d528272a643ad',1,'bmp280_driver_t']]],
  ['read8bits_2',['Read8Bits',['../structbmp280__driver__t.html#af08dd03ffc83f1571a4a6c950cf4f84b',1,'bmp280_driver_t']]],
  ['register_20addresses_3',['register addresses',['../group___d_e_f___r_e_g___a_d_d_r.html',1,'']]],
  ['registers_4',['Coeffs registers',['../group___d_e_f___c_o_e_f_f_s.html',1,'']]],
  ['resolution_5',['Temperature resolution',['../group___d_e_f___t_e_m_p.html',1,'']]]
];
